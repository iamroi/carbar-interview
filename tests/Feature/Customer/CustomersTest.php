<?php

use App\Models\User;
use App\Services\Stripe;
use Laravel\Jetstream\Features;
use Illuminate\Foundation\Testing\WithFaker;
use function Pest\Faker\faker;

/**
 *
 */
test('customers can be created', function () {
    $this->actingAs($user = User::factory()->create());

    // create a customer
    $customerData = [
        'name' => faker()->name,
        'email' => faker()->email,
        'address' => [
            'city' => faker()->city,
        ],
    ];

    $response = $this->post(route('customers.store'), $customerData);

    // retrieve the same customer
    $stripeResponse = Stripe::instance()->customers()->retrieve($response->getSession()->get('new_customer_id'));

    // finally, assert that the customer was created
    expect($customerData['name'])->toBe($stripeResponse->name);
    expect($customerData['email'])->toBe($stripeResponse->email);
    expect($customerData['address']['city'])->toBe($stripeResponse->address->city);
});

/**
 *
 */
test('customers can be updated', function () {
    $this->actingAs($user = User::factory()->create());

    // create a customer
    $customerData = [
        'name' => faker()->name,
        'email' => faker()->email,
        'address' => [
            'city' => faker()->city,
        ],
    ];

    $createCustomerResponse = $this->post(route('customers.store'), $customerData);
    $stripeCustomerId = $createCustomerResponse->getSession()->get('new_customer_id');

    // update the same customer
    $updatedCustomerData = [
        'name' => 'updated-'.$customerData['name'],
        'email' => 'updated-'.$customerData['email'],
        'address' => [
            'city' => 'updated-'.$customerData['address']['city'],
        ],
    ];

    $updateCustomerResponse = $this->put(
        route('customers.update', ['customer' => $stripeCustomerId]),
        $updatedCustomerData
    );

    // retrieve the same customer
    $customerResponse = Stripe::instance()->customers()->retrieve($stripeCustomerId);

    // finally, assert that the customer was updated
    expect($updatedCustomerData['name'])->toBe($customerResponse->name);
    expect($updatedCustomerData['email'])->toBe($customerResponse->email);
    expect($updatedCustomerData['address']['city'])->toBe($customerResponse->address->city);
});

/**
 *
 */
test('customers can be deleted', function () {
    $this->actingAs($user = User::factory()->create());

    // create a customer
    // TODO refactor this to a helper
    $customerData = [
        'name' => faker()->name,
        'email' => faker()->email,
        'address' => [
            'city' => faker()->city,
        ],
    ];

    $createCustomerResponse = $this->post(route('customers.store'), $customerData);
    $stripeCustomerId = $createCustomerResponse->getSession()->get('new_customer_id');

    $deleteCustomerResponse = $this->delete(
        route('customers.destroy', ['customer' => $stripeCustomerId])
    );

    // retrieve the same customer
    $customerResponse = Stripe::instance()->customers()->retrieve($stripeCustomerId);

    // finally, assert that the customer was deleted
    expect($customerResponse->deleted)->toBe(true);
});
