const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@': path.resolve('resources/js'),
        },
    },
    // ref - https://inertiajs.com/client-side-setup?frontend=svelte&backend=laravel#code-splitting
    output: {
        chunkFilename: 'js/[name].js?id=[chunkhash]',
    },
    // plugins: [
    //     new webpack.DefinePlugin({ "global.GENTLY": false })
    // ],
    // devServer: {
    //     proxy: {
    //         host: '0.0.0.0',  // host machine ip
    //         port: 8080,
    //     },
    //     watchOptions:{
    //         aggregateTimeout: 200,
    //         poll: 5000
    //     },
    // },
};
