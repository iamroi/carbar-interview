<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      class="h-full bg-gray-100">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        @routes
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased h-full">
        @inertia

{{--        @if (app()->environment() === 'local')--}}
{{--            <script id="__bs_script__">//<![CDATA[--}}
{{--                document.write("<script async src='http://localhost:9090/browser-sync/browser-sync-client.js?v=2.27.5'><\/script>".replace("HOST", location.hostname));--}}
{{--                //]]></script>--}}
{{--        @endif--}}
    </body>
</html>
