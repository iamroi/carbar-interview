import _has from 'lodash/has'
import { usePage } from '@inertiajs/inertia-vue3'
import {computed} from "vue";

export default {
    install: (app, options) => {
        const errorBags = computed(() => usePage().props.value.errorBags)

        const errorFromBag = (field, errorBag = 'default') => {
            if (! errorBags.value[errorBag]) {
                return null;
            }
            if (errorBags.value[errorBag].hasOwnProperty(field)) {
                return errorBags.value[errorBag][field][0];
            }
            return null;
        }

        app.$errorFromBag = errorFromBag
        app.config.globalProperties.$errorFromBag = errorFromBag

        const getFormFieldErrorsFromResponse = errors => {
            let fieldErrors = errors;
            if (_has(errors, 'response')) {
                let apiData = {};
                apiData = _has(errors.response, 'data') ? errors.response.data : apiData;
                apiData = _has(errors.response, 'data.data') ? errors.response.data.data : apiData;

                // if(errors.response) {
                // errorResponse = errors.response;
                fieldErrors = {}; // resetting first!! if a plain response sent
                fieldErrors = _has(errors.response, 'data.message') ? {message: errors.response.data.message} : fieldErrors
                fieldErrors = _has(apiData, 'errors') ? apiData.errors : fieldErrors
                fieldErrors = _has(apiData, 'messages') ? apiData.messages : fieldErrors
                fieldErrors = _has(apiData, 'error.fields') ? apiData.error.fields : fieldErrors
            } /*else if (_has(errors, 'data')) {
    // if(errors.response) {
    // errorResponse = errors.response;
    fieldErrors = {}; // resetting first!! if a plain response sent
    fieldErrors = _has(errors.data, 'message') ? errors.response.data : fieldErrors
    // fieldErrors = _has(errors.response, 'data.error.fields') ? errors.response.data.error.fields : fieldErrors
}*/

// console.log(errors.response)
// console.log(errorResponse)
// console.log(fieldErrors)
            return fieldErrors;
        }

        const getMissingFormFieldErrorsFromResponse = (errors, form) => {
            const that = this

            let fieldErrors = app.$getFormFieldErrorsFromResponse(errors);
            let missingFieldErrors = [];

            Object.keys(fieldErrors).forEach(function (key) {
                // console.log(form)
                // console.log(key)
                // console.log(form[key])
                // if (!form[key]) {
                if (!_has(form, key)) {
                    // that.$bvToast.toast(errors[key], {
                    //     solid: true,
                    //     noCloseButton: true,
                    //     toaster: 'b-toaster-top-right',
                    //     autoHideDelay: 3500,
                    //     variant: 'danger',
                    //     ...toastOptions,
                    // })
                    missingFieldErrors.push(
                        fieldErrors[key]
                    )
                }
            })

            return missingFieldErrors;
        }

        const catchMissingFormFieldErrorsAndToast = (errors, form, toastOptions = {}) => {
            const that = this

            let missingFieldErrors = getMissingFormFieldErrorsFromResponse(errors, form);

            missingFieldErrors.forEach(function (errorMessage) {
                // console.log({app})
                app.$appNotify({
                    message: errorMessage,
                    duration: 3500,
                    // duration: 500000,
                    type: 'error',
                    // type: 'warning',
                    ...toastOptions,
                })
            })

            return missingFieldErrors;
        }

        app.$getFormFieldErrorsFromResponse = getFormFieldErrorsFromResponse
        app.config.globalProperties.$getFormFieldErrorsFromResponse = getFormFieldErrorsFromResponse

        app.$getMissingFormFieldErrorsFromResponse = getMissingFormFieldErrorsFromResponse
        app.config.globalProperties.$getMissingFormFieldErrorsFromResponse = getMissingFormFieldErrorsFromResponse

        app.$catchMissingFormFieldErrorsAndToast = catchMissingFormFieldErrorsAndToast
        app.config.globalProperties.$catchMissingFormFieldErrorsAndToast = catchMissingFormFieldErrorsAndToast
    }
}
