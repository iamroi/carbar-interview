import {
    onBeforeUnmount
} from 'vue'
import { TinyEmitter } from 'tiny-emitter';

const eventEmitter = new TinyEmitter()

export default function useEventBus () {

    const eventHandlers = []

    const offEmitters = () => {
        console.log('offEmitters')
        eventHandlers.forEach((eventHandler) =>
            eventEmitter.off(eventHandler.event, eventHandler.handler)
        )
    }

    // onBeforeUnmount(offEmitters)

    return {
        onEvent (event, handler) {
            eventHandlers.push({ event, handler })
            eventEmitter.on(event, handler)
        },
        offEvent (event, handler) {
            eventHandlers.forEach((eventHandler) => {
                if(event !== eventHandler.event) return
                eventEmitter.off(eventHandler.event, eventHandler.handler)
            })
        },
        emitEvent (event, payload) {
            eventEmitter.emit(event, payload)
        },
        offEmitters
    }
}
