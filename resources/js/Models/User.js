import Model from '@/Models/Model.js'

export default class User extends Model {
    primaryKey() {
        return 'id'
    }
}
