import { Model as BaseModel } from 'vue-api-query'

// Model.$http = ctx.$axios

export default class Model extends BaseModel {
    // Define a base url for a REST API
    baseURL() {
        return ''
    }

    parameterNames () {
        const defaultParams = super.parameterNames()
        const customParams = {
            page: 'page[number]',
            limit: 'page[size]'
        }

        return { ...defaultParams, ...customParams }
    }

    // Implement a default request method
    request(config) {
        return this.$http.request(config)
    }
}
