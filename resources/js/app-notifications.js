import useEventBus from "@/Modules/useEventBus";
const { emitEvent, onEvent } = useEventBus()

export default {
    install: (app, options) => {

        const appNotify = function(options) {
            if(options.method === 'banner') {
                emitEvent('show-banner-message', {
                    message: options.message,
                })
                return
            }

            app.$toast.show(options.message, {
                position: 'top-right',
                // // duration: 3500,
                duration: 2500,
                // duration: 500000,
                type: 'info',
                ...options,
            });
        }

        app.$appNotify = appNotify
        app.config.globalProperties.$appNotify = appNotify
    }
}
