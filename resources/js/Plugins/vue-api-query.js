import { Model } from 'vue-api-query'
import { usePage } from '@inertiajs/inertia-vue3'

export default {
    install: (app, options) => {
        // console.log(app.$http.defaults)
        // app.$http.defaults.headers.common.Accept = 'application/json'

        // app.$http.onRequest(config => {
        //     console.log({config})
        // })

        // {
        //     header: {
        //         'Content-Type': 'application/json'
        //     }
        // }

        Model.$http = app.$http
    }
}
