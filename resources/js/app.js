require('./bootstrap');

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import axios from 'axios'
import VueAxios from 'vue-axios'
import Toaster from "@meforma/vue-toaster";
import AppNotifications from '@/app-notifications.js'
import FormHelpers from '@/form-helpers.js'
import VueApiQuery from '@/Plugins/vue-api-query.js'
import PageHeading from '@/Components/PageHeading.vue'
import PageBody from '@/Components/PageBody.vue'

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Carbar';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: name => import(`./Pages/${name}.vue`),
    setup({ el, app, props, plugin, }) {
        const vueApp = createApp({ render: () => h(app, props) })
            .use(plugin)
            .use(VueAxios, axios)
            .use(VueApiQuery)
            .use(FormHelpers)
            .use(AppNotifications)
            .use(Toaster)
            .mixin({ methods: { route } })

        vueApp.component('PageHeading', PageHeading);
        vueApp.component('PageBody', PageBody);

        return vueApp.mount(el);
    },
});

InertiaProgress.init({ color: '#4B5563' });
