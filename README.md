![alt text](https://www.carbar.com.au/resources/images/core/carbar-logo.png)

## Installation
- `composer install`
- `yarn`
- Create an empty file named `database.sqlite` under `database/` directory
- `php artisan migrate:fresh --seed`
- Copy `.env.example` to `.env`
- Please update env values `STRIPE_KEY` and `STRIPE_SECRET` from your Stripe account

## Development
- `yarn hot`
- In another terminal, `php artisan serve`
- Go to `http://localhost:8000`

## Authentication
Please use the below credentials to login to the application.

- Goto [Login page](http://localhost:8000/login)
- *Username:* `superadmin@superadmin.com`
- *Password:* `h2c=wWz-W6qr-?$v`

## Stripe related files
- app/Providers/StripeServiceProvider.php
- app/Services/Stripe.php
- config/stripe.php

## Tests
Please run `./vendor/bin/pest`
