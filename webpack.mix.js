const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

if (! mix.inProduction()) {
    // const host = process.env.APP_URL.split('//')[1];
    const host = 'localhost';

    mix.options({
        hmrOptions: {
            host: host,
            port: 9090,
        }
    });

    mix.browserSync({
        proxy: false,
        domain: 'localhost',
        port: 9090,
        open: false,
        ghostMode: false,
        notify: false,
        socket: {
            domain: `localhost:${9090}`
            // domain: `${config.DEVELOPMENT_URL}:${config.PORT}`
        },
    });
}

mix.js('resources/js/app.js', 'public/js').vue()
    .postCss('resources/css/app.css', 'public/css', [
        require('postcss-import'),
        require('postcss-nested'),
        require('tailwindcss'),
    ])
    .webpackConfig(require('./webpack.config'));

if (mix.inProduction()) {
    mix.version();
}

mix.disableNotifications();
