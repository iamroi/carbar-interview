<?php

namespace App\Services;

class Stripe
{
    public $client;

    private $apiUri;

    public function __construct(array $options = [])
    {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => 'https://api.stripe.com/v1/',
            'auth' => [$options['api_key'] ?? config('stripe.secret'), ''],
            'verify' => false,
        ]);
    }

    /**
     * @return Stripe
     */
    public static function instance()
    {
        return new self;
    }

    /**
     * Set the customers URI
     */
    public function customers()
    {
        $this->apiUri = 'customers';

        return $this;
    }

    /**
     * Send GET request for listing resources
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all($options)
    {
        $response = $this->client->request('GET', $this->apiUri, ['query' => $options]);

        return json_decode($response->getBody());
    }

    /**
     * Send POST request to create a new resource
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create($options)
    {
        $response = $this->client->request('POST', $this->apiUri, ['query' => $options]);

        return json_decode($response->getBody());
    }

    /**
     * Send GET request to retrieve a resource
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function retrieve($key, $options = [])
    {
        $response = $this->client->request('GET', "{$this->apiUri}/{$key}", ['query' => $options]);

        return json_decode($response->getBody());
    }

    /**
     * Send POST request to update a resource
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($key, $options = [])
    {
        $response = $this->client->request('POST', "{$this->apiUri}/{$key}", ['query' => $options]);

        return json_decode($response->getBody());
    }

    /**
     * Send DELETE request to destroy a resource
     *
     * @param $options
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete($key, $options = [])
    {
        $response = $this->client->request('DELETE', "{$this->apiUri}/{$key}", ['query' => $options]);

        return json_decode($response->getBody());
    }
}
