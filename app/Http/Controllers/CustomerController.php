<?php

namespace App\Http\Controllers;

use App\Http\Requests\Customer\CustomerRequest;
use App\Services\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class CustomerController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $options = [
            'limit' => 5,
        ];

        if($request->starting_after) {
            $options['starting_after'] = $request->starting_after;
        }

        if($request->ending_before) {
            $options['ending_before'] = $request->ending_before;
        }

        try {
            $stripeResponse = Stripe::instance()->customers()->all($options);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors(['error' => $e->getMessage()]);
        }

        return $this->renderJsonOrInertia('Customer/Index', [
            'customers' => $stripeResponse->data,
            'limit' => $options['limit'],
            'hasMore' => $stripeResponse->has_more,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return Inertia::render('Customer/Create', []);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CustomerRequest $request)
    {
        try {
            $stripeResponse = Stripe::instance()->customers()->create($request->validated());
        } catch (\Exception $e) {
            return Redirect::back()->withErrors(['error' => $e->getMessage()]);
        }

        return Redirect::route('customers.edit', $stripeResponse->id)
            ->with('flash.success', "Customer {$request->name} created.")
            ->with('new_customer_id', $stripeResponse->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($stripeId)
    {
        try {
            $stripeResponse = Stripe::instance()->customers()->retrieve($stripeId);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors(['error' => $e->getMessage()]);
        }

        return Inertia::render('Customer/Edit', [
            'customer' => $stripeResponse,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(CustomerRequest $request, $stripeId)
    {
        try {
            $stripeResponse = Stripe::instance()->customers()->update($stripeId, $request->validated());
        } catch (\Exception $e) {
            return Redirect::back()->withErrors(['error' => $e->getMessage()]);
        }

        return Redirect::back()->with('flash.success', 'Customer updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy($stripeId)
    {
        try {
            $stripeResponse = Stripe::instance()->customers()->delete($stripeId);
        } catch (\Exception $e) {
            return Redirect::back()->withErrors(['error' => $e->getMessage()]);
        }

        return Redirect::back()->with('flash.success', 'Customer deleted successfully.');
    }
}
