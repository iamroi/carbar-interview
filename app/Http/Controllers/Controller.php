<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Inertia\Inertia;
use MarcinOrlowski\ResponseBuilder\ResponseBuilder;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function renderJsonOrInertia($component, $data) {
        if (request()->hasHeader('X-Inertia') == false && request()->expectsJson()) {
            return response()->json($data);
        }

        return Inertia::render($component, $data);
    }

    function isInertia() {
        if (request()->hasHeader('X-Inertia') == false && request()->expectsJson()) {
            return false;
        }

        return true;
    }
}
