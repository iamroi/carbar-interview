<?php

namespace App\Http\Requests\Customer;

use App\Enums\UserAccountStatus;
use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Spatie\Enum\Laravel\Rules\EnumRule;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            'name' => ['required', 'string', 'max:50'],
            'email' => ['required', 'email', 'max:50'],
            'address.city' => ['string', 'max:50'],
            'phone' => ['string', 'max:10'],
        ];
    }

    public function attributes()
    {
        return[
        ];
    }
}
