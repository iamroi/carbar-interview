#!/bin/bash

composer install
yarn
php artisan migrate:fresh --seed
php artisan config:clear && php artisan cache:clear >/dev/null 2>&1
