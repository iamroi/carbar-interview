<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class DevUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create a default admin user
        $admin = new User();
        $admin->f_name = 'Super Administrator';
        $admin->email = 'superadmin@superadmin.com';
        $admin->password = \Hash::make('h2c=wWz-W6qr-?$v');
        $admin->email_verified_at = now();
        $admin->save();
        //$admin->assignRole('superadmin');

        \App\Models\User::factory()
            ->count(10)->create();;
    }
}
