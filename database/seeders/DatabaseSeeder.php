<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //DEVELOPMENT ONLY SEEDS
        if (app()->environment() !== 'production') {
            $this->call(DevUsersSeeder::class);
        }
    }
}
